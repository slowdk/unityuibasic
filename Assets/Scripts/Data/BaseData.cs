﻿using UnityEngine;
using System.Collections;

public class BaseData
{
    public uint id;
    public uint UIid;
    public string name;

    public BaseData()
    {
        id = 0;
        UIid = 0;
        name = null;
    }
}
