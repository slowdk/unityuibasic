﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System;

public class ItemDataWeapon:ServerDataWeapon
{
    public string imgPath;

    public ItemDataWeapon(string csvUIStr, ServerData srvData = null)
    {
        string[] values = csvUIStr.Split(',');

        uiid = uint.Parse(values[0]);
        itemName = values[1];
        imgPath = values[2];

        SetServerData(srvData);
    }

    public void SetServerData(ServerData srvData)
    {
        if (srvData != null)
        {
            Type srvDataType= srvData.GetType();
            FieldInfo[] srvProps = srvDataType.GetFields(BindingFlags.Public | BindingFlags.Instance);
            object value;
            
            for (int i=0; i<srvProps.Length; i++)
            {
                value = srvProps[i].GetValue(srvData);

                if (isValidData(value)) {
                    srvProps[i].SetValue(this, value);
                }
            }
        }
    }

    private bool isValidData(object value)
    {
        if (Convert.ToInt32(value) == 0)
        {
            return false;
        }
        return true;
    }
}
