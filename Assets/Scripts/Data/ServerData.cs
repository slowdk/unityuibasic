﻿using UnityEngine;
using System.Collections;

public class ServerData
{
    public uint id;
    public uint uiid;
    public string itemName;
    public uint price;
    public PRICE_TYPE priceType;
    public SALE_TYPE saleType;

    public ServerData()
    {
        id = 0;
        uiid = 0;
        price = 0;
        priceType = PRICE_TYPE.NONE;
        saleType = SALE_TYPE.NONE;
    }
}
