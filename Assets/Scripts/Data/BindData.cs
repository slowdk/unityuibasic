﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BindData
{
    public delegate void Listener(BaseData data);

    public string bindName;
    public BaseData data;
    public List<Listener> listeners;

    public BindData(string bindName, BaseData data, Listener fn=null)
    {
        listeners = new List<Listener>();

        this.bindName = bindName;
        this.data = data;
        this.listeners.Add(fn);
    }

    public void SetData(BaseData data)
    {

    }

    public void addListener(Listener fn)
    {
        Listener duplicated = listeners.Find(f => f == fn);
        
        if (duplicated == null)
        {
            listeners.Add(fn);
        }
    }
}
