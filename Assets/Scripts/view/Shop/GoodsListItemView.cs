﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoodsListItemView : MonoBehaviour {

    public Image itemImg;
    public Text itemName;
    public Text props;
    public Text itemPrice;

    ItemDataWeapon itemData;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetData(object data)
    {
        itemData = (ItemDataWeapon)data;

        if (itemData != null)
        {
            itemImg.sprite = UIDataManager.instance.getItemImg(itemData.imgPath);
            itemName.text = itemData.itemName;
            props.text = "Damage: " + itemData.dmg + "\nSpeed: " + itemData.speed;
            itemPrice.text = itemData.price.ToString() + Utils.getPriceUnit(itemData.priceType);
        }
    }
}
