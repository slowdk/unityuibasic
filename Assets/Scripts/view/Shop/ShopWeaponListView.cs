﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopWeaponListView : DataListener {

    public Transform listItemFab;

	// Use this for initialization
	void Start () {
        init();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    override public void SetData(object data)
    {
        base.SetData(data);
        List<ItemDataWeapon> itemList = (List<ItemDataWeapon>)data;
        ScrollRect scrollRect = GetComponent<ScrollRect>();
        GoodsListItemView listItem;
        Transform obj;

        if (itemList != null && scrollRect != null)
        {
            foreach(ItemDataWeapon itemData in itemList)
            {
                if (isValidGoods(itemData))
                {
                    obj = Instantiate(listItemFab, scrollRect.content);
                    listItem = obj ? obj.GetComponent<GoodsListItemView>() : null;

                    if (listItem != null)
                    {
                        listItem.SetData(itemData);
                    }
                }
            }
        }
    }

    private bool isValidGoods(ServerData data)
    {
        if (data.saleType == SALE_TYPE.SHOP)
        {
            return true;
        }
        return false;
    }
}
