﻿using UnityEngine;
using System.Collections;
using System;

public class AssetManager
{
    Sprite[] itemImages;

    public AssetManager()
    {
        itemImages = Resources.LoadAll<Sprite>("itemTileSet");
    }

    public Sprite getItemImg(string path)
    {
        return Array.Find(itemImages, sp => sp.name == path);
    }
}
