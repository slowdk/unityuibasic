﻿using UnityEngine;
using System.Collections;

/**
 * 데이터가 필요한 UI 객체에 추가한 후, BindName을 등록해 놓으면 해당 데이터가 갱신될 때마다 데이터를 받습니다.
 */
public class DataListener : MonoBehaviour
{
    public string bindName;
    public object data;

    // Use this for initialization
    void Start()
    {
        init();
    }

    protected void init()
    {
        if (bindName == null)
        {
            Debug.Log("DataBinding: No BindName.");
        }

        UIDataManager.instance.AddBindListener(bindName, this);
    }

    private void OnDestroy()
    {
        if (UIDataManager.instance != null)
        {
            UIDataManager.instance.RemoveBindListener(bindName, this);
        }
    }

    public virtual void SetData(object data)
    {
        this.data = data;
    }
}
