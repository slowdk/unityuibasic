﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class UIDataManager : MonoBehaviour {

    public static UIDataManager instance;

    AssetManager assetMng;
    Dictionary<string, List<DataListener>> dataListeners;
    Dictionary<string, object> dataCont;

    private void Awake()
    {
        instance = this;
        assetMng = new AssetManager();
        dataListeners = new Dictionary<string, List<DataListener>>();
        dataCont = new Dictionary<string, object>();
    }

    // Use this for initialization
    void Start () {
        
        // dummy UI data
        SetRawUIData("shop.weapon",
            "1,Weapon1,itemTileSet_1\n" +
            "2,Weapon2,itemTileSet_2\n" +
            "3,Weapon3,itemTileSet_3\n" +
            "4,Weapon4,itemTileSet_4\n" +
            "5,Weapon5,itemTileSet_5\n" +
            "6,Weapon6,itemTileSet_6\n" +
            "7,Weapon7,itemTileSet_7\n" +
            "8,Weapon8,itemTileSet_8\n" +
            "9,Weapon9,itemTileSet_9\n" +
            "10,Weapon10,itemTileSet_10\n" +
            "11,Weapon11,itemTileSet_11\n" +
            "12,Weapon12,itemTileSet_12\n" +
            "13,Weapon13,itemTileSet_13\n"
        );

        // dummy Server data
        List<ServerData> srvDataList = new List<ServerData>();

        for (uint i=0; i<10; i++)
        {
            srvDataList.Add(new ServerDataWeapon
            {
                id = i + 1,
                uiid = i + 1,
                price = (i + 1) * 1000,
                priceType = PRICE_TYPE.GOLD,
                saleType = SALE_TYPE.SHOP,
                dmg = (int)(i + 1) * 10,
                speed = (int)i
            });
        }
        SetServerData("shop.weapon", srvDataList);
    }

    public void SetRawUIData(string bindName, string csvStr)
    {
        string[] lines = csvStr.Split(new string[]{ "\n" }, StringSplitOptions.None);
        List<ItemDataWeapon> itemList = new List<ItemDataWeapon>();

        foreach (string line in lines)
        {
            if (line.Length > 0)
            {
                itemList.Add(new ItemDataWeapon(line));
            }
        }

        dataCont.Add(bindName, itemList);
    }

    public void SetServerData(string bindName, List<ServerData> srvDataList)
    {
        List<ItemDataWeapon> itemList = (List<ItemDataWeapon>)dataCont[bindName];
        ItemDataWeapon tmpItemData;

        foreach (ServerData srvData in srvDataList)
        {
            tmpItemData = itemList.Find(itemData => itemData.uiid == srvData.uiid);

            if (tmpItemData != null)
            {
                tmpItemData.SetServerData(srvData);
            }
        }

        DispatchData(bindName);
    }

    public void AddBindListener(string bindName, DataListener listener, bool getDataNow = true)
    {
        bool isContained = dataListeners.ContainsKey(bindName);
        List<DataListener> listenerList;

        if (isContained)
        {
            listenerList = dataListeners[bindName];
        }
        else
        {
            dataListeners[bindName] = listenerList = new List<DataListener>();
        }

        DataListener duplicated = listenerList.Find(storedListener => storedListener == listener);

        if (duplicated == null)
        {
            listenerList.Add(listener);
        }

        if (getDataNow)
        {
            DispatchData(bindName, listener);
        }
    }

    public void RemoveBindListener(string bindName)
    {
        List<DataListener> listenerList = dataListeners[bindName];
        listenerList.RemoveRange(0, listenerList.Count);
    }

    public void RemoveBindListener(string bindName, DataListener listener)
    {
        List<DataListener> listenerList = dataListeners[bindName];
        listenerList.Remove(listener);
    }

    private void DispatchData(string bindName)
    {
        List<DataListener> listenerList = dataListeners[bindName];

        if (listenerList == null) return;

        object data = dataCont[bindName];

        foreach (DataListener listener in listenerList)
        {
            listener.SetData(data);
        }
    }

    private void DispatchData(string bindName, DataListener listener)
    {
        bool isContained = dataCont.ContainsKey(bindName);

        if (isContained)
        {
            object data = dataCont[bindName];
            listener.SetData(data);
        }
    }

    public Sprite getItemImg(string path)
    {
        return assetMng.getItemImg(path);
    }
}
