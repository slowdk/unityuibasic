﻿using UnityEngine;
using System.Collections;

public class Utils
{
    public static string getPriceUnit(PRICE_TYPE priceType)
    {
        switch(priceType)
        {
            case PRICE_TYPE.GOLD: return "G";
            case PRICE_TYPE.CASH: return "C";
        }
        return "";
    }

    public static bool IsNumber(object value)
    {
        return value is sbyte
                || value is byte
                || value is short
                || value is ushort
                || value is int
                || value is uint
                || value is long
                || value is ulong
                || value is float
                || value is double
                || value is decimal;
    }
}
